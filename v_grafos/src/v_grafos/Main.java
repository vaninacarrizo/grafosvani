package v_grafos;

//import grafo.Grafo;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Grafo_Dirigido g = new Grafo_Dirigido(5);

		g.agregarArco(0, 1);
		g.agregarArco(1, 2);
		g.agregarArco(1, 3);
		g.agregarArco(0, 3);
		g.agregarArco(3, 4);
		
		System.out.print("Existe arista 0,1 ");
		System.out.println(g.existeArco(0, 1));
		System.out.print("Existe arista 1,0 ");
		System.out.println(g.existeArco(1,0));
		
		System.out.print("Existe arista 1,2 ");
		System.out.println(g.existeArco(1,2));
		System.out.print("Existe arista 2,1 ");
		System.out.println(g.existeArco(2,1));
		
		System.out.print("Existe arista 1,3 ");
		System.out.println(g.existeArco(1,3));
		System.out.print("Existe arista 3,1 ");
		System.out.println(g.existeArco(3,1));
		
		System.out.print("Existe arista 0,3 ");
		System.out.println(g.existeArco(0,3));
		System.out.print("Existe arista 3,0 ");
		System.out.println(g.existeArco(3,0));
		
		System.out.print("Existe arista 3,4 ");
		System.out.println(g.existeArco(3,4));
		System.out.print("Existe arista 4,3 ");
		System.out.println(g.existeArco(4,3));
		
//		System.out.print("Existe arista 4,47 ");
//		System.out.println(g.existeArco(4,47));
		System.out.print("Existe arista -1,4 ");
		System.out.println(g.existeArco(-1,4));
		
		

	}

}
