package v_grafos;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class Grafo_Dirigido {

	private ArrayList<Set<Integer>> _vecinos;
	private int _vertice; 
	
	public Grafo_Dirigido( int vertices)
	{
		_vecinos = new ArrayList<Set<Integer>>(vertices);

		for (int i = 0; i < vertices; i++)
			_vecinos.add(new HashSet<Integer>());	

		_vertice = vertices;
	}
	
	public void agregarArco ( int i, int j)
	{
		verificarArco(i, j, "agregar");
		
		_vecinos.get(i).add(j);
//		_vecinos.get(j).add(i);
	}
	public void eliminarArista(int i, int j)
	{
		verificarArco(i, j, "eliminar");
		
		_vecinos.get(i).remove(j);
//		_vecinos.get(j).remove(i);

	}
	
	public boolean existeArco (int i, int j)
	{
		verificarArco(i, j, "consultar");
		
		return _vecinos.get(i).contains(j);
	}

	public Set<Integer> vecinos(int i)
	{
		verificarVertice(i, " un vertice ");
		
		return _vecinos.get(i);
	}

	public int grado (int i)
	{ 
		return _vecinos.get(i).size();
	}
		
	private void verificarArco(int i, int j, String tipo) 
	{
		if (i == j)
			throw new IllegalArgumentException("Se intento "+tipo+" una arista con i=j : "+i +"/"  + j);
		
		verificarVertice(i, tipo);
		verificarVertice(j, tipo);

	}

	private void verificarVertice(int i, String tipo) 
	{
		if (i < 0 || i >= _vertice)
			throw new IllegalArgumentException("Se intento usar "+tipo+" con valores, fuera de rango: "+ i);
	}

	public int vertices()
	{
		return _vertice;
	}
	
	
	public boolean esClique( Set<Integer> conjunto) 
	{
		if (conjunto == null)
			throw new IllegalArgumentException("El conjunto no puede ser null");

		for (int v: conjunto)
		   verificarVertice(v," Clique");
		
		if (conjunto.isEmpty())
			return true;
		
		for (int v: conjunto)
		for (int otro: conjunto) 
			if (v != otro)
				if (existeArco(v,otro) == false)
					return false;
		
		return true;
	}

	
	
	
	
//	Caudal m�ximo de cada arco = problema de flujo m�ximo
//	Nodo origen= conectado a todos los productores (lista) de nodos y cantidad de flujo (no se puede agrear)
//	Nodo productor= m�ximo que pueden enviar (oferta)
//	Nodo Consumidor= m�ximo que pueden consumir (demanda)
//	Nodo de Paso= sin l�mite, depende de los arcos
//	Nodo destino= a �l se llega desde todos los consumidores (lista) de nodos y cantidad de flujo (no se puede agregar)
//	
//	1.Agregar un nodo especificando su tipo y su maximo
//	2.Agregar un arco especificando qu� nodos conecta(A y B) y su capacidad m�xima
//	3.Consultar el flujo m�ximo agregando un nodo origen y uno destino. Usar 
	
//	-Flujo: sumo el flujo de los primeros nodos
//	-capacidad: sumo la capacidad de los arcos que unen el subgrafo considerado con el resto del grafo total
}
